#!/usr/bin/python
import xpybutil.event as event
import xpybutil.ewmh as ewmh
import xpybutil.util as util
import xpybutil.window
import xpybutil

currentActiveWindow = None
xpybutil.window.listen(xpybutil.root, 'PropertyChange')
while True:
    e = xpybutil.conn.wait_for_event()

    aname = util.get_atom_name(e.atom)
    if aname == '_NET_ACTIVE_WINDOW':
        newActive = ewmh.get_active_window().reply()
        if newActive == 0:
            continue

        try:
            xpybutil.window.listen(currentActiveWindow, )
        except :
            print('failed to stop listening to', currentActiveWindow)

        currentActiveWindow = newActive
        try:
            xpybutil.window.listen(currentActiveWindow, 'PropertyChange')
            print("new window", currentActiveWindow, ewmh.get_wm_name(currentActiveWindow).reply())
        except Exception as e:
            print('failed to listen to window', currentActiveWindow, e)
    elif aname == 'WM_NAME':
        print("new name", currentActiveWindow, ewmh.get_wm_name(currentActiveWindow).reply())
